package ir.webutils;

import java.util.*;
import java.io.*;

/**
 * Node in the the Graph data structure.
 *
 * @see Graph
 *
 * @author Ray Mooney
 */
public class Node {

  /**
   * Name of the node.
   */
  String name;
  double pageRank;
  double e;

  /**
   * Lists of incoming and outgoing edges.
   */
  List<Node> edgesOut = new ArrayList<Node>();
  List<Node> edgesIn = new ArrayList<Node>();


  /**
   *    Getter and Setter for pageRank
   */
   public double getPageRank()
   {
        return pageRank;
   }
   public void setPageRank(double rank)
   {
        this.pageRank = rank;
   }
   
   /**
   *    Getter and Setter for pageRank
   */
   public double getE()
   {
        return e;
   }
   public void setE(double e)
   {
        this.e = e;
   }
   
  /**
   * Constructs a node with that name.
   */
  public Node(String name) {
    this.name = name;
  }

  /**
   * Adds an outgoing edge
   */
  public void addEdge(Node node) {
    edgesOut.add(node);
    node.addEdgeFrom(this);
  }

  /**
   * Adds an incoming edge
   */
  void addEdgeFrom(Node node) {
    edgesIn.add(node);
  }

  /**
   * Returns the name of the node
   */
  public String toString() {
    return name;
  }

  /**
   * Gives the list of outgoing edges
   */
  public List<Node> getEdgesOut() {
    return edgesOut;
  }

  /**
   * Gives the list of incoming edges
   */
  public List<Node> getEdgesIn() {
    return edgesIn;
  }
}
